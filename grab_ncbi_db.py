from Bio import SeqIO, Entrez
from urllib2 import URLError, HTTPError
import urllib2
import argparse
import time

def seq_checker(in_data,in_db):
    in_seqs = []
    with open(in_data,"r") as infile:
        for line in infile:
            if "." in line:
                line = line.split(".")[0]
            in_seqs.append(line.replace("\n",""))
    chunks = [in_seqs[x:x+200] for x in xrange(0, len(in_seqs), 200)]
    return(chunks)

def long_recovery(start,stop,sid,tdb):
    attempt = 0
    while attempt < 3:
        attempt += 1
        try:
            print "Attempting to fetch long sequence %s: %s,%s"%(sid,start,stop)
            fetched = Entrez.efetch(db=tdb,
                                    rettype="fasta",
                                    retmode="text",
                                    id = sid,
                                    seq_start = int(start),
                                    seq_stop = int(stop))
            print "Fetch Successful"
            break
        except URLError as err:
            print("Received error from server %s" % err)
            print("Attempt %i of 3" % attempt)
            time.sleep(15)
    time.sleep(1)
    return(fetched)

def translate(i,seq_snip,nseq,lnseq,found,re_move,re_place):
    for item in i['GBSeq_feature-table']:
        for l_item in item['GBFeature_quals']:
            if "coded_by" in l_item.values():
                op = l_item['GBQualifier_value']
                trans = False
                if "complement" in op: trans = True
                for re in re_move:  op = op.replace(re,"")
                for re in re_place: op = op.replace(re," ")
                if "join" in op:
                    op = op.replace("join(","")
                    op = op.replace(",%s"%op.split(" ")[0],"")
                op = op.split(" ")
                if len(op) > 1:
                    seq_snip.setdefault(i['GBSeq_primary-accession'],[op[0],op[1:],trans])
                else:
                    seq_snip.setdefault(i['GBSeq_primary-accession'],[op[0],trans])
                if int(op[1]) < 100000:
                    nseq.append(op[0])
                else:
                    lnseq.append([i['GBSeq_primary-accession'],op[0],trans])
                found = True
    return([seq_snip,nseq,lnseq,found])

def creepy_grabbler(in_seqs,in_db,out_db,output):
    chunk = 0
    Entrez.email = "face@book.com"
    quick_core = {}
    seq_snip = {}
    longseqs = []

    
    while chunk < len(in_seqs):
        time.sleep(2)
        print "posting %i acc numbers"%len(in_seqs[chunk])
        print in_seqs[chunk]
        print "CHUNK NUMBER",chunk+1,"/",len(in_seqs)
        attempt = 0
        while attempt < 3:
            attempt += 1
            try:
                post = Entrez.read(Entrez.epost("protein",id=",".join(in_seqs[chunk])))
            except:
                print("Received error from server")
                time.sleep(5)
        webenv = post["WebEnv"]
        query_key = post["QueryKey"]
        attempt = 0
        while attempt < 3:
            attempt += 1
            try:
                print "Attempting to fetch the chunk"
                fetched = Entrez.efetch(db=in_db,
                                         rettype="gb",
                                         retmode="xml",
                                         webenv=webenv,
                                         query_key=query_key)
                print "Fetch successful"
                break
            except HTTPError as err:
                        print("Received error from server %s" % err)
                        print("Attempt %i of 3" % attempt)
                        time.sleep(15)
        xx = Entrez.parse(fetched)
        nseq = []
        lnseq = []
        re_move = ['<','>','complement(',')']
        re_place = [":",".."]

        
        for i in xx:
            found = False
            seq_snip,nseq,lnseq,found = translate(i,seq_snip,nseq,lnseq,found,re_move,re_place)
            if found == False:
                print "Lost %s, attempting xref recovery"%i['GBSeq_primary-accession']
                recovery = i['GBSeq_source-db'].split("xrefs: ")[1].split("xrefs")[0].split(",")
                for acc_id in recovery:
                    time.sleep(0.5)
                    acc_id = acc_id.replace(" ","")
                    try:
                        print "trying",acc_id
                        handle = Entrez.efetch(db=in_db,
                                               rettype="gb",
                                               retmode="xml",
                                               id = acc_id)
                        ll = Entrez.parse(handle)
                        for l in ll:
                            seq_snip,nseq,lnseq,found = translate(l,seq_snip,nseq,lnseq,found,re_move,re_place)
                        seq_snip[i['GBSeq_primary-accession']] = seq_snip[acc_id.split(".")[0]]
                        print "Recovery succeeded. Consider using %s in the future"%acc_id
                        break
                    except:
                        continue
                if found == False:
                    print "Recovery for %s failed. Please remove or manually fix the ACC id: %s"%(i['GBSeq_accession-version'],i['GBSeq_accession-version'])      
                    raise ValueError("No Match for ACC # %s"%i['GBSeq_primary-accession'])

                
        print "Posting %i translated ACC_ID's to NCBI"%len(nseq)
        if len(nseq) > 0:
            attempt = 0
            while attempt < 3:
                attempt += 1
                try:
                    post = Entrez.read(Entrez.epost("nuccore",id=",".join(nseq)))
                    break
                except:
                    print("Received error from server")
                    time.sleep(5)
            webenv = post["WebEnv"]
            query_key = post["QueryKey"]
            attempt = 0
            while attempt < 3:
                attempt += 1
                try:
                    print "Attempting to fetch translated chunk"
                    fetched = Entrez.efetch(db=out_db,
                                            rettype="fasta",
                                            retmode="text",
                                            webenv=webenv,
                                            query_key=query_key)
                    print "Fetch Successful"
                    break
                except HTTPError as err:
                    print("Received error from server %s" % err)
                    print("Attempt %i of 3" % attempt)
                    time.sleep(15)
            print "Organizing translated sequences"
            data = fetched.read()
            with open("tmp_data","a") as outfile:  outfile.write(data)

            
        for seq in lnseq:
            seqdata = {}
            fetched = long_recovery(seq_snip[seq[0]][1][0],seq_snip[seq[0]][1][-1],seq[1],out_db)
            data = fetched.read()
            for line in data.split("\n"):
                if ">" in line:
                    seqdata.setdefault(line,"")
                    x = line
                else:
                    seqdata[x] = seqdata[x]+line.replace("\n","")
            with open("tmp_data","a") as outfile: outfile.write(data)
        chunk+=1
        longseqs = longseqs+lnseq

                     
    print "Writing"
    with open(output,"w") as outfile:
        data = SeqIO.parse("tmp_data","fasta")
        for seq in data:
            quick_core.setdefault(seq.id.split("-")[0],[seq.description,seq.seq])
        for i in [x for sublist in in_seqs for x in sublist]:
            k = seq_snip[i][0]
            if i in [x[0] for x in longseqs]:
                k = seq_snip[i][0]+":"+seq_snip[i][1][0]
                minus = max(int(seq_snip[i][1][0])-1,1)
                for checkpt in enumerate(seq_snip[i][1]):
                    seq_snip[i][1][checkpt[0]] = int(seq_snip[i][1][checkpt[0]])-int(minus)
            else:
                k = seq_snip[i][0]
            name = quick_core[k][0]
            itr = 0
            seq = ""
            for l in range(len(seq_snip[i][1])/2):
                seq = seq+quick_core[k][1][int(seq_snip[i][1][itr])-1:int(seq_snip[i][1][itr+1])]
                itr+=2
            if seq_snip[i][2] == True:
                seq = seq.reverse_complement()
            outfile.write(">%s__%s\n%s\n"%(name,i,seq))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = "Grab the source sequence from a different NCBI database using ACC numbers")
    parser.add_argument('-i', metavar = 'input', type = str, required = True,
                        help = "Valid genbank IDs separated by a newline character.")
    parser.add_argument('-t', metavar = 'to db', type = str, required = True, choices= ['nuccore','protein'],
                        help = "Desired db of outputs.")
    parser.add_argument('-f', metavar = 'from db', type = str, required = True, choices= ['nuccore','protein'],
                        help = "DB of inputs.")
    parser.add_argument('-o', metavar = 'output', type = str, required = True,
                        help = "Full path of desired output file.")
    args = parser.parse_args()
    in_seqs = seq_checker(args.i,args.f)
    outfile = open("tmp_data","w")
    outfile.close()
    creepy_grabbler(in_seqs,args.f,args.t,args.o)
