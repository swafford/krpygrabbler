KRPYGrabbler v0.0.1

###BETA WARNING###
This script is still in development and though it has been tested in both windows and linux OSes, you should verify that outputs
are correct.  Please report errors and bugs to andrew@swafford.com

====README BEGIN====
KRPYGrabbler is designed to retrieve the nucleotide CDS from a list of protein acc #s and vice versa. This is particularly useful for performing
analyses of selection on protein databases that do not have nucleotide CDS acc #s available.  The script will only work on acc #s found in
genbank.

====USAGE====

-i path to file with valid genbank protein acc #'s separated by a newline character "\n"
-t the database, either 'nuccore' or 'protein', to translate the existing data into
-f the database your are translating from, either 'nuccore' or 'protein'
-o the desired path to place the output file